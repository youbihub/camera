#include "camera/camera.h"

#include <doctest/doctest.h>

#include "opencv2/imgcodecs.hpp"
// #include <opencv2/highgui.hpp>

#include "iostream"
// #include "shaper/signal.h"

namespace camera {

  DOCTEST_TEST_CASE("Camera Parameters Load") {
    auto path = "test/data/camera/cam_load_test.json";
    auto calibration = Calibration(path);
    DOCTEST_CHECK(calibration.focal_[0] == 1.1);
    DOCTEST_CHECK(calibration.focal_[1] == 2.2);
    DOCTEST_CHECK(calibration.center_[0] == 3.3);
    DOCTEST_CHECK(calibration.center_[1] == 4.4);
    DOCTEST_CHECK(calibration.distortion_[0] == 5.5);
    DOCTEST_CHECK(calibration.distortion_[1] == 6.6);
    DOCTEST_CHECK(calibration.distortion_[2] == 7.7);
    DOCTEST_CHECK(calibration.distortion_[3] == 8.8);
    DOCTEST_CHECK(calibration.distortion_[4] == 9.9);
  }
  DOCTEST_TEST_CASE("Camera Projection") {
    DOCTEST_SUBCASE("Point in front") {
      auto path = "test/data/camera/cam_load_test.json";
      auto calibration = Calibration(path);
      auto x = ProjectPoint<double>({321.123, 0., 0.}, calibration);
      auto y = Eigen::Vector2d{3.3, 4.4};
      DOCTEST_CHECK(x == y);
    }
  }
  //   DOCTEST_TEST_CASE("Virtual Image") {
  //     // todo: something not a signal
  //     auto contour = signal::MakeSignal("test/data/signal/H.signal").contour.discreet;
  //     auto path = "test/data/camera/cam-identity.json";
  //     auto calibration = Calibration(path);
  //     DOCTEST_SUBCASE("signal in front") {
  //       auto Tcs = Eigen::Matrix4d();
  //       // clang-format off
  //         Tcs <<  0, 0, -1, 10,
  //                -1, 0,  0, 0,
  //                 0, 1,  0, 0,
  //                 0, 0,  0, 1;
  //       // clang-format on
  //       auto frame = camera::VirtualFrame(contour, calibration, Tcs);
  // #if GITLABCI == 0
  //       DOCTEST_CHECK(cv::imwrite("test/imgout/Hvfront.png", frame));
  // #endif
  //     }
  //   }
}  // namespace camera