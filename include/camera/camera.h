#pragma once

#include <Eigen/Dense>  // for Vector, Matrix3Xd, Matrix4d, Matr...
#include <array>        // for array

#include "opencv2/core.hpp"
// #include "opencv2/imgproc.hpp"
// #include <opencv2/core/mat.hpp>  // for Mat
#include <string>  // for string
namespace camera {
  /**
   * @brief A structure to store camera calibration parameters. Compliant with OpenCv
   * specifications.
   */
  struct Calibration {
    /**
     * @brief Construct a new Calibration object
     *
     * @param path path of the json setting file
     */
    explicit Calibration(const std::string& path);
    constexpr static int n_kp = 5;
    std::array<double, n_kp> distortion_ = std::array<double, n_kp>();  ///< k1, k2, p1, p2, k3
    std::array<double, 2> focal_ = std::array<double, 2>();             ///< fx, fy
    std::array<double, 2> center_ = std::array<double, 2>();            ///< cx, cy
    std::array<int, 2> resolution_ = std::array<int, 2>();              ///< rx, ry
  };

  cv::Mat VirtualFrame(const Eigen::Matrix3Xd& discreet_contour, Calibration calibration,
                       const Eigen::Matrix4d& Tcs);

  /**
   * @brief
   *
   * @tparam T
   * @param point_signal_d 3d Point in camera coordinates (x-front, y-left, z-up)
   * @param calibration Camera calibration
   * @return Eigen::Matrix<T, 2, 1> 2d Point in frame coordinates (O-topleft, x-right, y-down)
   */
  template <typename T> auto ProjectPoint(const Eigen::Vector<T, 3>& point_signal_d,
                                          const Calibration& calibration) -> Eigen::Vector<T, 2> {
    // todo: vectorize
    auto& px = point_signal_d(0);
    auto& py = point_signal_d(1);
    auto& pz = point_signal_d(2);

    const T x = -py / px;
    const T y = -pz / px;

    const double& k1 = calibration.distortion_[0];
    const double& k2 = calibration.distortion_[1];
    const double& p1 = calibration.distortion_[2];
    const double& p2 = calibration.distortion_[3];
    const double& k3 = calibration.distortion_[4];

    const T r2 = x * x + y * y;
    const T r4 = r2 * r2;
    const T r6 = r4 * r2;

    T distortion_k = 1. + r2 * k1 + r4 * k2 + r6 * k3;

    const double& fx = calibration.focal_[0];
    const double& fy = calibration.focal_[1];
    const double& cx = calibration.center_[0];
    const double& cy = calibration.center_[1];

    auto pix_cam = Eigen::Vector<T, 2>();
    // pix_cam(0) = x * distortion_k;
    // pix_cam(1) = y * distortion_k;

    const T xy = x * y;
    auto tang_coeff = 2.;
    pix_cam(0) = x * distortion_k + tang_coeff * p1 * xy + p2 * (r2 + tang_coeff * x * x);
    pix_cam(1) = y * distortion_k + tang_coeff * p2 * xy + p1 * (r2 + tang_coeff * y * y);

    pix_cam(0) = pix_cam(0) * fx + cx;
    pix_cam(1) = pix_cam(1) * fy + cy;

    return pix_cam;
  }
  // struct ProjectedPoints {
  //   Eigen::Matrix2Xd proj_points;
  //   Eigen::Array<bool, 1, Eigen::Dynamic> in_front;
  // };

  Eigen::Matrix2Xd FromSignToImage(const Eigen::Matrix3Xd& p_s, const Eigen::Matrix4d& Tcs,
                                  const Calibration& calibration);

}  // namespace camera
