#include "camera/camera.h"

#include <Eigen/src/Core/ArithmeticSequence.h>      // for ArithmeticSequence
#include <Eigen/src/Core/Assign.h>                  // for MatrixBase::opera...
#include <Eigen/src/Core/Block.h>                   // for Block
#include <Eigen/src/Core/CwiseNullaryOp.h>          // for CwiseNullaryOp
#include <Eigen/src/Core/DenseBase.h>               // for DenseBase, DenseB...
#include <Eigen/src/Core/DenseCoeffsBase.h>         // for DenseCoeffsBase
#include <Eigen/src/Core/GeneralProduct.h>          // for MatrixBase::opera...
#include <Eigen/src/Core/MatrixBase.h>              // for MatrixBase
#include <Eigen/src/Core/Product.h>                 // for Product
#include <Eigen/src/Core/util/IndexedViewHelper.h>  // for makeIndexedViewCo...
#include <Eigen/src/Core/util/IntegralConstant.h>   // for cleanup_index_typ...
#include <Eigen/src/Core/util/Memory.h>             // for first_aligned
#include <Eigen/src/Core/util/Meta.h>               // for enable_if<>::type
// #include <opencv2/core/hal/interface.h>             // for CV_8UC3

#include <cstddef>            // for size_t
#include <fstream>            // for ifstream, istream
#include <memory>             // for allocator_traits<...
#include <new>                // for operator new
#include <nlohmann/json.hpp>  // for basic_json<>::obj...

#include "opencv2/core.hpp"
#include "opencv2/imgproc.hpp"
// #include <opencv2/core/mat.inl.hpp>  // for Mat::Mat, Mat::~Mat
// #include <opencv2/core/types.hpp>    // for Point
// #include <opencv2/imgproc.hpp>       // for fillPoly
#include <utility>  // for move
#include <vector>   // for vector

// #include "../plugins/IndexedViewMethods.h"  // for DenseBase::operat...
#include "Eigen/src/Core/DenseBase.h"  // for DenseBase::col
#include "camera/camera.h"
// #include "ceres/jet.h"  // for Jet

namespace camera {

  Calibration::Calibration(const std::string& path) {
    auto file_storage = nlohmann::json(nullptr);
    {
      auto i = std::ifstream(path, std::ios_base::in);
      i >> file_storage;
    }

    center_[0] = file_storage["calibration"]["center"]["x"];
    center_[1] = file_storage["calibration"]["center"]["y"];
    focal_[0] = file_storage["calibration"]["focal"]["x"];
    focal_[1] = file_storage["calibration"]["focal"]["y"];
    distortion_[0] = file_storage["calibration"]["distortion"]["k1"];
    distortion_[1] = file_storage["calibration"]["distortion"]["k2"];
    distortion_[2] = file_storage["calibration"]["distortion"]["p1"];
    distortion_[3] = file_storage["calibration"]["distortion"]["p2"];
    distortion_[4] = file_storage["calibration"]["distortion"]["k3"];
    resolution_[0] = file_storage["calibration"]["resolution"]["width"];
    resolution_[1] = file_storage["calibration"]["resolution"]["height"];
    // calibration.fps_ = file_storage["calibration"]["fps"];
  }

  Eigen::Matrix2Xd FromSignToImage(const Eigen::Matrix3Xd& p_s, const Eigen::Matrix4d& Tcs,
                                   const Calibration& calibration) {
    // auto proj_points{ProjectedPoints{}};
    auto n = size_t(p_s.cols());
    auto p_s1 = Eigen::Matrix4Xd(4, n);
    p_s1(Eigen::seq(0, 2), Eigen::all) = p_s;
    p_s1.row(3) = Eigen::VectorXd::Ones(n);
    auto p_c = (Tcs * p_s1)(Eigen::seq(0, 2), Eigen::all);
    // proj_points.in_front = p_c.row(0).array() > 0;
    auto p_i = Eigen::Matrix2Xd(2, n);
    // todo functional
    for (size_t i = 0; i < n; i++) {
      if (p_c(0, i) < 0) {
        p_i.col(i) << std::nan("1"), std::nan("1");
      } else {
        p_i.col(i) = camera::ProjectPoint<double>(p_c.col(i), calibration);
      }
    }
    return p_i;
  }
  auto VirtualFrame(const Eigen::Matrix3Xd& discreet_contour, Calibration calibration,
                    const Eigen::Matrix4d& Tcs) -> cv::Mat {
    const auto& p_s = discreet_contour;
    auto p_i = FromSignToImage(p_s, Tcs, calibration);
    auto maxchar = 255.;
    auto white = cv::Scalar{maxchar, maxchar, maxchar};
    auto frame = cv::Mat(calibration.resolution_[1], calibration.resolution_[0], CV_8UC3, white);
    std::vector<std::vector<cv::Point> > pp(1);
    size_t n = discreet_contour.cols();
    for (size_t i = 0; i < n; i++) {
      pp[0].push_back({static_cast<int>(p_i(0, i)), static_cast<int>(p_i(1, i))});
    }
    cv::fillPoly(frame, pp, {0, 0, 0});
    return frame;
  }

  // // explicit template instanciation
  // template Eigen::Matrix<double, 2, 1> ProjectPoint<double>(
  //     const Eigen::Matrix<double, 3, 1>& point_signal_d, const Calibration& calibration);
  // using J1 = ceres::Jet<double, 1>;
  // const auto dof_6_and_t = 6 + 1;
  // using J7 = ceres::Jet<double, dof_6_and_t>;
  // template Eigen::Matrix<J1, 2, 1> camera::ProjectPoint<J1>(
  //     const Eigen::Matrix<J1, 3, 1>& point_signal_d, const Calibration& calibration);
  // template Eigen::Matrix<J7, 2, 1> camera::ProjectPoint<J7>(
  //     const Eigen::Matrix<J7, 3, 1>& point_signal_d, const Calibration& calibration);
}  // namespace camera